<?php

namespace Drupal\commerce_payrexx\PluginForm;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;

class PayrexxRedirectCheckoutForm extends BasePaymentOffsiteForm {

	/**
	 * {@inheritdoc}
	 */
	public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
		$form = parent::buildConfigurationForm($form, $form_state);
		$configuration = $this->getConfiguration();

		/** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
		$payment = $this->entity;

		$payrexx = new \Payrexx\Payrexx($configuration['instance_name'], $configuration['secret']);
		$gateway = new \Payrexx\Models\Request\Gateway();

		// amount multiplied by 100
		$gateway->setAmount(($payment->getAmount()->getNumber() + ($payment->getAmount()->getNumber() * $configuration['fee'] / 100)) * 100);
		// currency ISO code
		$gateway->setCurrency($payment->getAmount()->getCurrencyCode());
		// VAT rate percentage (nullable)
		$gateway->setVatRate($configuration['vat']);
		//success and failed url in case that merchant redirects to payment site instead of using the modal view
		$gateway->setSuccessRedirectUrl($form['#return_url']);
		$gateway->setFailedRedirectUrl($form['#cancel_url']);
		// optional: payment service provider(s) to use (see http://developers.payrexx.com/docs/miscellaneous)
		// empty array = all available psps
		$gateway->setPsp([]);
		// optional: whether charge payment manually at a later date (type authorization)
		$gateway->setPreAuthorization(false);
		// optional: whether charge payment manually at a later date (type reservation)
		$gateway->setReservation(false);
		// optional: reference id of merchant (e. g. order number)
		$gateway->setReferenceId($this->createOrderId());

		try {
			$response = $payrexx->create($gateway);
		} catch (\Payrexx\PayrexxException $e) {
			print $e->getMessage();
			exit;
		}

		return $this->buildRedirectForm(
			$form,
			$form_state,
			$response->getLink(),
			[],
			PaymentOffsiteForm::REDIRECT_POST
		);
	}

	/**
	 * Build the order id taking order prefix into account.
	 *
	 * @return string
	 */
	private function createOrderId() {
		/** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
		$payment = $this->entity;

		$configuration = $this->getConfiguration();
		$order_id = $payment->getOrderId();

		// Ensure that Order number is at least 4 characters otherwise QuickPay will reject the request.
		if (strlen($order_id) < 4) {
			$order_id = substr('000' . $order_id, -4);
		}

		return $order_id;
	}

	/**
	 * @return array
	 */
	private function getConfiguration() {
		/** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
		$payment = $this->entity;

		/** @var \Drupal\commerce_quickpay_gateway\Plugin\Commerce\PaymentGateway\RedirectCheckout $payment_gateway_plugin */
		$payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
		return $payment_gateway_plugin->getConfiguration();
	}

}