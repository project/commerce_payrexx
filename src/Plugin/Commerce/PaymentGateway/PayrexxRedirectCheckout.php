<?php

namespace Drupal\commerce_payrexx\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Payrexx offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "payrexx_redirect_checkout",
 *   label = @Translation("Payrexx (Redirect to Payrexx)"),
 *   display_label = @Translation("Payrexx"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_payrexx\PluginForm\PayrexxRedirectCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */

class PayrexxRedirectCheckout extends OffsitePaymentGatewayBase {

	public function defaultConfiguration() {
		return [
			'instance_name' => '',
			'secret' => '',
			'vat' => '',
			'fee' => '',
		] + parent::defaultConfiguration();
	}

	public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
		$form = parent::buildConfigurationForm($form, $form_state);
		$form['instance_name'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Instance name'),
			'#description' => $this->t('This instance name from your Payrexx account.'),
			'#default_value' => $this->configuration['instance_name'],
			'#required' => TRUE,
		];
		$form['secret'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Secret'),
			'#description' => $this->t('The secret from your Payrexx account.'),
			'#default_value' => $this->configuration['secret'],
			'#required' => TRUE,
		];
		$form['vat'] = [
			'#type' => 'number',
			'#title' => $this->t('VAT'),
			'#min' => 0,
			'#step' => 0.01,
			'#description' => $this->t('The VAT percentage.'),
			'#default_value' => $this->configuration['vat'],
			'#required' => TRUE,
		];
		$form['fee'] = [
			'#type' => 'number',
			'#title' => $this->t('Fee'),
			'#min' => 0,
			'#step' => 0.01,
			'#description' => $this->t('The Fee percentage.'),
			'#default_value' => $this->configuration['fee'],
			'#required' => TRUE,
		];
		return $form;
	}

	public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
		parent::submitConfigurationForm($form, $form_state);
		$values = $form_state->getValue($form['#parents']);
		$this->configuration['instance_name'] = $values['instance_name'];
		$this->configuration['secret'] = $values['secret'];
		$this->configuration['vat'] = $values['vat'];
		$this->configuration['fee'] = $values['fee'];
	  }
}